import React, {useState} from 'react'


function Cell(props){

	const [action, setAction] = useState('');
	let actionUser = '';

	function setActionUser (id){
		if(props.lastAction === '' || props.lastAction === '0'){
			props.setHeader('Ходит нолик!');
			actionUser = 'X';
		}else if(props.lastAction === 'X'){
			props.setHeader('Ходит крестик!');
			actionUser = '0';
		}
		if (action === '') {
			setAction(actionUser);
			props.setLastAction(actionUser);	
		}
		props.arr[id] = actionUser;
		let isWin = isWinner(props.arr);
		if (isWin === 'X'){
			props.setHeader('Крестик победил!!!');
		}else if(isWin === '0'){
			props.setHeader('Нолик победил!!!');
		}
	}
	
	return (
		<div className = 'row__cell' 	
			id = {props.cellId} 
			onClick = {(el)=>setActionUser(el.target.id)}>
			<p>{action}</p>
		</div>
	)
}

function isWinner(arr1){
	let arr = [
    [0,1,2],
    [3,4,5],
    [6,7,8],
    [0,3,6],
    [1,4,7],
    [2,5,8],
    [0,4,8],
    [2,4,6]
  ];
  
  
  let arrX = [];
  let arr0 = [];
  
  for (let i = 0; i < arr1.length; i++){
    if( arr1[i] === 'X'){
       arrX.push(i);   
    }else if(arr1[i] === '0'){
      arr0.push(i);  
    }
  }
  
  for (let i = 0; i < arr.length; i++){
    let x = arrX.filter(el => arr[i].includes(el));
    let y = arr0.filter(el => arr[i].includes(el));
    if (x.length == 3){
      return 'X';
    }else if(y.length == 3){
      return '0';
    }
  }
  return '';
}

export default Cell;